FROM openjdk:17
ADD demo/target/*.jar ms-applydigital.jar
EXPOSE 9000
ENTRYPOINT ["java", "-jar", "ms-applydigital.jar"]
