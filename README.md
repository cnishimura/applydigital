# DEMO APPLY DIGITAL
The following was discovered as part of building this project:

* The JVM level was changed from  '17', review the [JDK Version Range](https://github.com/spring-projects/spring-framework/wiki/Spring-Framework-Versions#jdk-version-range) on the wiki for more details.

# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [This will create a Swagger API documentation interface that can be accessed] (http://localhost:port/swagger-ui.html) once the application is running.

### Guides
The following guides illustrate how to use some features concretely:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/rest/)


### Project run settings
* Config Java 17 on intelliJ: Project Structure
* Install and configure the following applications on your system
    * Postgresql
* Include the following environment variables in the gradle test configuration (Test in 'applydigital-ms') / Environment Variable
    * MS_PROFILE=DB_PORT=5432;DB_USER=postgres;DB_NAME=applydigital-ms;DB_HOST=host.docker.internal;DB_PASSWORD=postgrespw
    * modify {postgresql_user, postgresql_pass} with your postgresql database credentials without "{}"

### Endpoints
This controller defines five endpoints:

* `GET /apply_digital/api/items/`: Retrieves a list of items with optional pagination, sorting, filtering by author, tags, and title, and searching by month. The results are filtered based on the currently authenticated user.
* `DELETE /apply_digital/api/items/{id}`: Deletes an existing item.

# NISHIMURA CASTRO CARLOS ANTONIO
### email: karnish24@gmail.com
### celular: +51 987539127
