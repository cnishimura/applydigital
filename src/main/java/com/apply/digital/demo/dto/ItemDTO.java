package com.apply.digital.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
@AllArgsConstructor
public class ItemDTO {
    private Long id;
    private String title;
    private String author;
    private String tags;
    private Date created_at;

    // constructors, getters, setters, and other methods omitted for brevity
}
