package com.apply.digital.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@ToString
@AllArgsConstructor
public class PageDTO {

    private List<ItemDTO> items;
    private int page;
    private int size;
    private long totalElements;
    private int totalPages;

    // constructors, getters, setters, and other methods omitted for brevity
}
