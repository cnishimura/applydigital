package com.apply.digital.demo.controller;

import com.apply.digital.demo.dto.PageDTO;
import com.apply.digital.demo.service.ItemService;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import io.swagger.annotations.Api;
import jakarta.servlet.http.HttpServletRequest;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import java.security.Key;

/**
 * ItemController class for defining REST endpoints and handling requests
 */
@RestController
@RequestMapping(value = "/api/items")
@AllArgsConstructor
@Api(value = "Items Management System")
public class ItemController {

    private final ItemService itemService;

    private static final String SECRET_KEY = "404E635266556A586E3272357538782F413F4428472B4B6250645367566B5970";

    /**
     * method for retrieving a page of items based on filtering and pagination parameters
     * @param author  Author filter
     * @param tags Tags filter
     * @param title Title filter
     * @param month Month filter
     * @param page Page number
     * @param size Page number
     * @param request Page size
     * @return
     */
    @GetMapping
    @ApiOperation(value = "Get items", notes = "Get items with pagination and filtering")
    public PageDTO getItems(
            @ApiParam(value = "Author filter") @RequestParam(required = false) String author,
            @ApiParam(value = "Tags filter") @RequestParam(required = false) String tags,
            @ApiParam(value = "Title filter") @RequestParam(required = false) String title,
            @ApiParam(value = "Month filter") @RequestParam(required = false) String month,
            @ApiParam(value = "Page number") @RequestParam(defaultValue = "0") int page,
            @ApiParam(value = "Page size") @RequestParam(defaultValue = "5") int size,
            HttpServletRequest request
    ) {

        validate(request) ;

        return itemService.getItems(author, tags, title, month, page, size);
    }

    /**
     * endpoint for deleting an item by ID
     * @param id Item id
     * @param request HttpServletRequest
     */
    @DeleteMapping("/{id}")
    @ApiOperation(value = "Remove item", notes = "Remove an item by id")
    public void deleteItem(@ApiParam(value = "Item id") @PathVariable Long id, HttpServletRequest request) {
        validate(request) ;
        itemService.deleteItem(id);
    }

    private Key getSignInKey() {
        byte[] keyBytes = Decoders.BASE64.decode(SECRET_KEY);
        return Keys.hmacShaKeyFor(keyBytes);
    }

    private void validate(HttpServletRequest request){
       /* String jwt = request.getHeader("Authorization").replace("Bearer ", "");
        Claims claims = Jwts.parser().setSigningKey(getSignInKey()).parseClaimsJws(jwt).getBody();*/
        // TODO: add more authentication/authorization checks here
    }
}
