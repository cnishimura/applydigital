package com.apply.digital.demo.entity;

import io.swagger.annotations.ApiModelProperty;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.Hibernate;

import java.util.Date;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
public class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The unique id of the book")
    private Long id;

    @Column(nullable = false)
    @ApiModelProperty(notes = "The title of the book")
    private String title;

    @Column(nullable = false)
    @ApiModelProperty(notes = "The author of the book")
    private String author;

    @Column(nullable = false)
    @ApiModelProperty(notes = "The tags of the book")
    private String tags;

    @Column(nullable = false,name = "created_at")
    @ApiModelProperty(notes = "The creation date of the book")
    private Date created;

    @Column(nullable = false)
    @ApiModelProperty(notes = "The deleted logical date of the book")
    private boolean deleted;

    // constructors, getters, setters, and other methods omitted for brevity lombok

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Item item = (Item) o;
        return id != null && Objects.equals(id, item.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
