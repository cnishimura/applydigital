package com.apply.digital.demo.repository;

import com.apply.digital.demo.entity.Item;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface ItemRepository extends JpaRepository<Item, Long>, JpaSpecificationExecutor<Item> {
    Object findByAuthor(String user1, Pageable any);

    Object findByTagsIn(List<String> eq, Pageable any);

    Optional<Item> findByCreatedAtBetween(Date init, Date end, Pageable any);


// method signatures for custom repository methods, such as findByAuthorAndTitleAndTags()
}
