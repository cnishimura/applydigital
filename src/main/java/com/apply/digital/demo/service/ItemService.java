package com.apply.digital.demo.service;

import com.apply.digital.demo.dto.ItemDTO;
import com.apply.digital.demo.dto.PageDTO;
import com.apply.digital.demo.entity.Item;
import com.apply.digital.demo.mapper.ItemMapper;
import com.apply.digital.demo.repository.ItemRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ItemService {

    private final ItemRepository itemRepository;

    // method for retrieving a page of items based on filtering and pagination parameters
    public PageDTO getItems(String author, String tags, String title, String month, int page, int size) {
        // build the specification based on the filtering parameters
        Specification<Item> spec = Specification.where(ItemSpecifications.hasNotDeleted());
        if (author != null) {
            spec = spec.and(ItemSpecifications.hasAuthor(author));
        }
        if (tags != null) {
            spec = spec.and(ItemSpecifications.hasTags(tags));
        }
        if (title != null) {
            spec = spec.and(ItemSpecifications.hasTitle(title));
        }
        if (month != null) {
            spec = spec.and(ItemSpecifications.hasMonth(month));
        }

        // retrieve the page of items using the repository and specification, and map them to DTOs
        Page<Item> itemPage = itemRepository.findAll(spec, PageRequest.of(page, size));
        List<ItemDTO> itemDTOs = itemPage.getContent().stream()
                .map(ItemMapper::toDTO)
                .collect(Collectors.toList());

        // map the page of items to a page DTO and return it
        return new PageDTO(itemDTOs, page, size, itemPage.getTotalElements(), itemPage.getTotalPages());
    }

    // method for deleting an item by ID
    public void deleteItem(Long id) {
        Optional<Item> itemOptional = itemRepository.findById(id);
        itemOptional.ifPresent(item -> {
            item.setDeleted(true);
            itemRepository.save(item);
        });
    }
}
