package com.apply.digital.demo.service;

import com.apply.digital.demo.entity.Item;
import jakarta.persistence.criteria.Path;
import org.springframework.data.jpa.domain.Specification;

import java.time.Year;
import java.util.Date;
import java.util.Locale;

// ItemSpecifications class for defining JPA specifications for filtering by author, tags, title, and month
public class ItemSpecifications {
    public static Specification<Item> hasAuthor(String author) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("author"), author);
    }
    public static Specification<Item> hasTags(String tags) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get("tags"), "%" + tags + "%");
    }
    public static Specification<Item> hasTitle(String title) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get("title"), "%" + title + "%");
    }
    public static Specification<Item> hasMonth(String month) {
        return (root, query, criteriaBuilder) -> {
            Path<Date> created_at = root.get("created_at");
            return criteriaBuilder.and(
                    criteriaBuilder.equal(criteriaBuilder.function("MONTHNAME", String.class, created_at),
                            month.toUpperCase(Locale.ENGLISH)),
                    criteriaBuilder.equal(criteriaBuilder.function("YEAR", Integer.class, created_at),
                            Year.now().getValue())
            );
        };
    }
    public static Specification<Item> hasNotDeleted() {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("deleted"), false);
    }
}

