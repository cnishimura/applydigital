package com.apply.digital.demo.mapper;

import com.apply.digital.demo.dto.ItemDTO;
import com.apply.digital.demo.entity.Item;

public class ItemMapper {
    public static ItemDTO toDTO(Item item) {
        return new ItemDTO(
                item.getId(),
                item.getTitle(),
                item.getAuthor(),
                item.getTags(),
                item.getCreated()
        );
    }
}
