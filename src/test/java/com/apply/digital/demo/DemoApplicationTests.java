package com.apply.digital.demo;

import com.apply.digital.demo.controller.ItemController;
import com.apply.digital.demo.entity.Item;
import com.apply.digital.demo.repository.ItemRepository;
import com.apply.digital.demo.util.JwtTokenUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ItemController.class)
class DemoApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ItemRepository itemRepository;

	@MockBean
	private JwtTokenUtil jwtTokenUtil;

	@Test
	public void testGetItems() throws Exception {

		List<Item> Items = Arrays.asList(
				new Item(1L, "Title 1", "Author 1", "Tags 1", new Date(), false),
				new Item(2L, "Title 2", "Author 2", "Tags 2", new Date(), false)
		);

		Page<Item> page = new PageImpl<>(Items);
		given(itemRepository.findByAuthor(eq("user1"), any(Pageable.class))).willReturn(page);
		given(jwtTokenUtil.getUsernameFromToken(anyString())).willReturn("user1");
		mockMvc.perform(get("/api/items").header("Authorization", "Bearer token"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.content", hasSize(2)))
				.andExpect(jsonPath("$.content[0].title", is("Title 1")))
				.andExpect(jsonPath("$.content[1].title", is("Title 2")));
	}

	@Test
	public void testGetItemById() throws Exception {
		Item item = new Item(1L, "Title", "Author", "Tags", new Date(), true);
		given(itemRepository.findById(eq(1L))).willReturn(Optional.of(item));
		mockMvc.perform(get("/api/items/1"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.title", is("Title")))
				.andExpect(jsonPath("$.content", is("Content")))
				.andExpect(jsonPath("$.author", is("Author")))
				.andExpect(jsonPath("$.tags", hasSize(2)))
				.andExpect(jsonPath("$.tags[0]", is("tag1")))
				.andExpect(jsonPath("$.tags[1]", is("tag2")));
	}

	@Test
	public void testCreateItem() throws Exception {
		String requestBody = "{ \"title\": \"Title\", \"content\": \"Content\", \"author\": \"Author\", \"tags\": [\"tag1\", \"tag2\"] }";
		given(jwtTokenUtil.getUsernameFromToken(anyString())).willReturn("user1");
		mockMvc.perform(post("/api/item").header("Authorization", "Bearer token").contentType(MediaType.APPLICATION_JSON)
						.content(requestBody))
				.andExpect(status().isCreated());
		verify(itemRepository, times(1)).save(any(Item.class));
	}

	@Test
	public void testUpdateItem() throws Exception {
		String requestBody = "{ \"title\": \"New Title\", \"content\": \"New Content\", \"tags\": [\"tag3\", \"tag4\"] }";
		Item item = new Item(1L, "Title", "Content", "Author", new Date(),true);
		given(itemRepository.findById(eq(1L))).willReturn(Optional.of(item));
		given(jwtTokenUtil.getUsernameFromToken(anyString())).willReturn("Author");
		mockMvc.perform(put("/api/Items/1").header("Authorization", "Bearer token").contentType(MediaType.APPLICATION_JSON)
						.content(requestBody))
				.andExpect(status().isOk());
		verify(itemRepository, times(1)).save(any(Item.class));
	}

	@Test
	public void testFilterItemsByAuthor() throws Exception {
		List<Item> Items = Arrays.asList(
				new Item(1L, "Title 1", "Content 1", "user1", new Date(), true),
				new Item(2L, "Title 2", "Content 2", "user1", new Date(),true)
		);
		Page<Item> page = new PageImpl<>(Items);
		given(itemRepository.findByAuthor(eq("user1"), any(Pageable.class))).willReturn(page);
		given(jwtTokenUtil.getUsernameFromToken(anyString())).willReturn("user1");
		mockMvc.perform(get("/api/Items?author=user1").header("Authorization", "Bearer token"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.content", hasSize(2)))
				.andExpect(jsonPath("$.content[0].title", is("Title 1")))
				.andExpect(jsonPath("$.content[1].title", is("Title 2")));
	}


	@Test
	public void testFilterItemsByTags() throws Exception {
		List<Item> items = Arrays.asList(
				new Item(1L, "Title 1", "Content 1", "Author 1", new Date(), true),
				new Item(2L, "Title 2", "Content 2", "Author 2", new Date(), true),
				new Item(3L, "Title 3", "Content 3", "Author 3", new Date(),true),
				new Item(4L, "Title 4", "Content 4", "Author 4", new Date(), true),
				new Item(5L, "Title 5", "Content 5", "Author 5", new Date(), true)
		);
		Page<Item> page = new PageImpl<>(items);
		given(itemRepository.findByTagsIn(eq(Arrays.asList("tag1", "tag2")), any(Pageable.class))).willReturn(page);
		given(jwtTokenUtil.getUsernameFromToken(anyString())).willReturn("user1");
		mockMvc.perform(get("/api/Items?tags=tag1").header("Authorization", "Bearer token"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.content", hasSize(4)))
				.andExpect(jsonPath("$.content[0].title", is("Title 1")))
				.andExpect(jsonPath("$.content[1].title", is("Title 2")))
				.andExpect(jsonPath("$.content[2].title", is("Title 3")))
				.andExpect(jsonPath("$.content[3].title", is("Title 4")));
	}

	@Test
	public void testSearchItemsByMonth() throws Exception {
		List<Item> Items = Arrays.asList(
				new Item(1L, "Title 1", "Content 1", "Author 1", getDate("2022-01-01"), true),
				new Item(2L, "Title 2", "Content 2", "Author 2", getDate("2022-02-01"), true),
				new Item(3L, "Title 3", "Content 3", "Author 3", getDate("2022-03-01"), true),
				new Item(4L, "Title 4", "Content 4", "Author 4", getDate("2021-12-01"), true),
				new Item(5L, "Title 5", "Content 5", "Author 5", getDate("2023-02-01"), true)
		);
		Page<Item> page = new PageImpl<>(Items);
		Date init = getDate("2022-01-01") ;
		Date end = getDate("2022-01-31");
		given(jwtTokenUtil.getUsernameFromToken(anyString())).willReturn("user1");
		mockMvc.perform(get("/api/Items?month=January").header("Authorization", "Bearer token"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.content", hasSize(1)))
				.andExpect(jsonPath("$.content[0].title", is("Title 1")));
	}

	private Date getDate(String dateString) {
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			return format.parse(dateString);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}
}
